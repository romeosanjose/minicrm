<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 08/08/2019
 * Time: 2:42 PM
 */

namespace App\Domain\User\Repository;


use App\Domain\Core\Repository\Doctrine\DoctrineRepository;
use App\Domain\User\Person;
use App\Domain\User\Repository\PersonRepository;


class PersonDoctrineRepository extends DoctrineRepository implements PersonRepository
{
    public function entity()
    {
        return Person::class;
    }

}
