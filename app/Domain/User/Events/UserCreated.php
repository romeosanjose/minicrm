<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 24/09/2019
 * Time: 2:07 PM
 */

namespace App\Domain\User\Events;

use App\Domain\Core\Entity\AbstractEntity;

class UserCreated extends AbstractEntity
{
    public function getEventName()
    {
        return UserCreated::class;
    }
}
