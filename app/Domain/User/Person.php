<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/03/2019
 * Time: 12:03 PM
 */

namespace App\Domain\User;


use App\Domain\Core\Entity\AbstractEntity;
use App\Domain\Core\Entity\Auditable;
use App\Domain\Core\Entity\AuditableTrait;
use App\Domain\Core\Entity\Identity;
use App\Domain\Core\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Person
 * @package App\Domain\User
 * @ORM\Entity
 * @ORM\Table(name="persons")
 */
class Person extends AbstractEntity implements Identity, Auditable
{
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $first_name;
    /**
     * @ORM\Column(type="string")
     */
    private $last_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Company\Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $phone;


    public function entityProperties()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'company'
        ];
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }


    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
        return $this;
    }

    /**
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $phone
     */
    public function setPhone(int $phone)
    {
        $this->phone = $phone;
    }


}
