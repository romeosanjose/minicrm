<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/06/2019
 * Time: 8:27 AM
 */

namespace App\Domain\User\UseCase;

use App\Domain\Core\Boundery\Request;
use App\Domain\Core\Boundery\Response;
use App\Domain\Core\UseCase\UseCase;
use App\Domain\User\Decorator\UserProtectedDecorator;
use App\Domain\User\Events\UserCreated;
use App\Domain\User\User;
use App\Events\Event;
use App\Domain\User\UseCase\GeneratePassword;
use Hackzilla\PasswordGenerator\Generator\HumanPasswordGenerator;
use App\Domain\Core\Validator\FormValidationException;

class AddUser extends UseCase
{
    protected $validationRules = [
        'email' => [
            'required',
            'email'
        ],
        'type' => [
            'required'
        ]
    ];

    /**
     * @param Request $request
     * @param User $createdBy
     * @return Response|mixed
     */
    public function addUser(Request $request, User $createdBy)
    {
        $data = $request->getData();

        if ($this->isExists($data['email'], 'email')) {
            throw new FormValidationException('Email already exists');
        }

        $user = (new User())->fromArray($data);
        $generatePasswordCommand = new GeneratePassword(new HumanPasswordGenerator());
        $password = $generatePasswordCommand->generatePassword();
        $user->setPassword($password);
        $user->setPasswordSend($password);
        $user = $this->encryptUserPassword($user, $user->getPassword());
        $user->setApiKey(bin2hex(openssl_random_pseudo_bytes(64)));
        $response = $this->addNewRecord(new Request($data), $user, $createdBy, new UserProtectedDecorator(), $validated = true);
        event(new UserCreated($user));

        return $response;
    }

    private function encryptUserPassword(User $user, $password)
    {
        $user->setSalt(sha1($password));
        $user->setPassword(sha1($password . $user->getSalt()));

        return $user;
    }

    private function isExists($needle, $key)
    {
        $userObjs = $this->repository->findBy([$key => $needle]);
        if (count($userObjs) > 0) {
            return true;
        }

        return false;
    }


}
