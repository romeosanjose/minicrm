<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 18/09/2019
 * Time: 9:48 PM
 */

namespace App\Domain\User\UseCase;

use App\Domain\Core\Boundery\Request;
use App\Domain\Core\Boundery\Response;
use App\Domain\User\Person;
use App\Domain\User\User;

class UpdateUser extends AddUser
{

    protected $validationRules = [
        'username' => [
            'required',
            'max_length(50)'
        ],
        'email' => [
            'required',
            'email'
        ],
        'type' => [
            'required'
        ]
    ];

    public function updateUser(Request $request, Person $person, User $updatedBy)
    {
        $userData = $request->getData();
        $user = $this->getRecord($userData['id']);
        unset($userData['person']);
        $user = $user->fromArray($userData);
        $user->setPerson($person);
        $user  = $this->setEditor($user, $updatedBy);

        return $this->repository->update($user, User::class);
    }
}
