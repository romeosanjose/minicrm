<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/03/2019
 * Time: 12:17 PM
 */


namespace App\Domain\User;


use App\Domain\Core\Entity\AbstractEntity;
use App\Domain\Core\Entity\Auditable;
use App\Domain\Core\Entity\AuditableTrait;
use App\Domain\Core\Entity\Identity;
use App\Domain\Core\Entity\IdentityTrait;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Class User
 * @package App\Domain\User
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends AbstractEntity implements Identity, Auditable, Authenticatable
{
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $salt;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $log_attempt = 0;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $locked = false;
    /**
    * @ORM\OneToOne(targetEntity="App\Domain\User\Person")
    * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
    **/
    private $person;

    /**
     * @ORM\Column(type="string")
     */
    private $api_key;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @return array
     */
    public function entityProperties()
    {
        return [
            'email',
            'log_attempt',
            'locked',
            'person',
            'type'

        ];
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogAttempt()
    {
        return $this->log_attempt;
    }

    /**
     * @param mixed $logAttempt
     */
    public function setLogAttempt($logAttempt)
    {
        $this->log_attempt = $logAttempt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param mixed $locked
     */
    public function setLocked(bool $locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    public function encryptPassword($password, $salt)
    {
        return sha1($password . $salt);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @param mixed $api_key
     */
    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;
    }

    public function authenticate($email, $password)
    {
        if ($email === $this->getEmail()){
            if ($this->encryptPassword($password, $this->getSalt()) === $this->getPassword()){
                return true;
            }
        }

        return false;
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }
}
