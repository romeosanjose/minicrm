<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/09/2019
 * Time: 4:43 PM
 */

namespace App\Domain\File\UseCase;


use App\Domain\Core\Boundery\Request;
use App\Domain\Core\UseCase\UseCase;
use App\Domain\File\Decorator\FileDecorator;
use App\Domain\File\File;
use App\Domain\User\User;
use Illuminate\Support\Facades\Storage;

class AddFile extends UseCase
{
    protected $validationRules = [
        'name' => [
            'required',
            'max_length(50)'
        ],
        'path' => [
            'required',
        ],
        'extension' => [
            'required',
        ],
        'size' => [
            'required',
        ],
    ];

    public function addFile(Request $request, User $createdBy)
    {
        $file = $request->getData();
        $savedFile = $file->store(config('minicrm.storage.profile_picture'));
        if ($savedFile) {
            $fileData = [
                'size' => $file->getClientSize(),
                'extension' => $file->getClientOriginalExtension(),
                'path' => $savedFile,
                'name' => $file->getClientOriginalName(),
            ];

            return$this->addNewRecord(new Request($fileData), new File(), $createdBy, new FileDecorator());
        }
    }
}
