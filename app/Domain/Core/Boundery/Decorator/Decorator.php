<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 13/03/2019
 * Time: 1:17 PM
 */

namespace App\Domain\Core\Boundery\Decorator;

use App\Domain\Core\Entity\AbstractEntity;

abstract class Decorator
{
    abstract public function decorate(AbstractEntity $entity);

}
