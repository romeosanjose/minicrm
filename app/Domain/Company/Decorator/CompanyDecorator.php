<?php


namespace App\Domain\Company\Decorator;


use App\Domain\Core\Entity\AbstractEntity;
use App\Domain\Core\Boundery\Decorator\Decorator;

class CompanyDecorator extends Decorator
{

    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }
}
