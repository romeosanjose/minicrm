<?php

namespace App\Domain\Company;


use App\Domain\Core\Entity\AbstractEntity;
use App\Domain\Core\Entity\Auditable;
use App\Domain\Core\Entity\AuditableTrait;
use App\Domain\Core\Entity\Identity;
use App\Domain\Core\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Company
 * @package App\Domain\Company
 * @ORM\Entity
 * @ORM\Table(name="companies")
 */

class Company extends AbstractEntity implements Identity, Auditable {
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToOne(targetEntity="App\Domain\File\File", mappedBy="Company")
     * @ORM\JoinColumn(name="logo_id", referencedColumnName="id")
     */
    private $logo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $website;


     public function entityProperties()
    {
        return [
            'name',
            'email',
            'logo',
            'website',
        ];
    }

    public function getName()
    {
    	return $this->name;
    }

    public function setName($name)
    {
    	$this->name = $name;
    }

    public function getEmail()
    {
    	return $this->email;
    }

    public function setEmail($email)
    {
    	$this->email = $email;
    }

    public function getLogo()
    {
    	return $this->logo;
    }

    public function setLogo($logo)
    {
    	$this->logo = $logo;
    }

    public function getWebsite()
    {
    	return $this->website;
    }

    public function setWebsite($website)
    {
    	$this->website = $website;
    }
}
