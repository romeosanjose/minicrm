<?php


namespace App\Domain\Company\Repository;

use App\Domain\Core\Repository\Doctrine\DoctrineRepository;
use App\Domain\Company\Company;

class CompanyDoctrineRepository extends DoctrineRepository implements CompanyRepository
{
    public function entity()
    {
        return Company::class;
    }

}
