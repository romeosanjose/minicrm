<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/05/2019
 * Time: 10:17 PM
 */

namespace App\Http\Controllers;


use App\Core\Services\ChurchService;
use App\Core\Services\SettingService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $churchService;
    protected $systemSettings;

    public function __construct(
        ChurchService $churchService,
        SettingService $settingService
    )
    {
        $this->churchService = $churchService;
        $this->systemSettings = $settingService;
    }

    public function showHomePage()
    {
        $texts = $this->systemSettings->getAllSettings();
        $churchServices = $this->churchService->getChurchServices();
        return view('home', [
            'church_services' => $churchServices,
            'text' => $texts
        ]);
    }


}