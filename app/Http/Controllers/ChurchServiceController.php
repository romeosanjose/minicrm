<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 22/05/2019
 * Time: 11:52 AM
 */

namespace App\Http\Controllers;

use App\Core\Services\ChurchService;

class ChurchServiceController extends Controller
{

    protected $churhService;

    public function __construct(
        ChurchService $churchService
    )
    {
        $this->churhService = $churchService;
    }

    public function getAll()
    {
        return $this->churhService->getChurchServices();
    }
}