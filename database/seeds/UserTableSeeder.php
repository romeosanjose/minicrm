<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personRepo = App::make(\App\Domain\User\Repository\PersonRepository::class);
        $userRepo = App::make(\App\Domain\User\Repository\UserRepository::class);


        $person = new \App\Domain\User\Person();
        $person->setId(0);
        $person->setUuid($personRepo->uuid());
        $person->setFirstName('admin1');
        $person->setLastName('admin1');
        $person = $personRepo->store($person);

        $user = new \App\Domain\User\User();
        $user->setId(0);
        $user->setUuid($userRepo->uuid());
        $user->setEmail('admin@admin.com');
        $password = 'password';
        $user->setSalt(sha1($password));
        $user->setPassword(sha1($password . $user->getSalt()));
        $user->setApiKey(bin2hex(openssl_random_pseudo_bytes(64)));
        $user->setPerson($person);
        $user->setType('ADMIN');
        $userRepo->store($user);
    }
}
