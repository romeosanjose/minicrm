<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 21/06/2019
 * Time: 4:22 PM
 */
use Tests\TestCase;

class AddUserTest extends TestCase
{
    /**
     * @dataProvider dataForUserCreate
     * @expectedException \App\Domain\Core\Validator\FormValidationException
     */
    public function testCreateExistingUserEmail($data)
    {
        $user = new \App\Domain\User\User();
        $user->setId(1);
        $user->setEmail("admin@mail.com");
        $mockRepo = $this->getMockBuilder(\App\Domain\User\Repository\UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockRepo->method("store")
            ->willReturn([]);
        $mockRepo->method("findBy")
            ->willReturn(['id' => 1]);    
        $request = new \App\Domain\Core\Boundery\Request($data);
        $userTxn = new \App\Domain\User\UseCase\AddUser($mockRepo);
        
        $result = $userTxn->addUser($request, $user);
        
    }

    public function dataForUserCreate()
    {
        return [
            [
                [
                    'password' => '@password',
                    'email'    => 'batman@hero.com',
                ]
            ]
        ];
    }


}
