<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/03/2019
 * Time: 12:46 PM
 */
use Tests\TestCase;

class UserTest extends TestCase
{
   public function testSuccessfulLogin()
   {
       $email = "super@mail.com";
       $password = "password123";
       $user = new \App\Domain\User\User();
       $user->setEmail($email);
       $user->setSalt(sha1($password));
       $user->setPassword($user->encryptPassword($password,  $user->getSalt()));
       $result = $user->authenticate(
           $email,
           $password
       );

       $this->assertTrue($result);
   }

   public function testInvalidLogin()
   {
    $email = "super@mail.com";
    $password = "password123";
    $user = new \App\Domain\User\User();
    $user->setEmail($email);
    $user->setSalt(sha1($password));
    $user->setPassword($user->encryptPassword($password,  $user->getSalt()));
    $result = $user->authenticate(
        $email,
        'passwordWrong12345'
    );

    $this->assertFalse($result);
   }
}
