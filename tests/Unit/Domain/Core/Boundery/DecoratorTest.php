<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 14/03/2019
 * Time: 1:58 PM
 */

use Tests\TestCase;

class DecoratorTest extends TestCase
{
    public $objectNames = [
        "User" => "User",
    ];

    public function testTransformObjectToStructure()
    {
        foreach($this->objectNames as $module => $objName) {
            $entityNameSpace = "App\\Domain\\$module\\$objName";
            $entity = new  $entityNameSpace;
            $entity->setId(1);

            $decoratorNameSpace = "App\\Domain\\$module\\Decorator\\$objName"."Decorator";
            $decorator = new $decoratorNameSpace($entity);
            $result = $decorator->decorate($entity);
            $this->assertTrue(is_array($result));
        }
    }
}
